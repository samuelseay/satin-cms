var express = require('express'),
    path = require('path'),
    coreApp = express();

module.exports = coreApp;

// view engine setup
coreApp.set('views', path.join(__dirname, 'views'));

//TODO separate out into a configurator with YAML
coreApp.set('view engine', 'jade');
coreApp.set('theme','default');

var adminAssetPath = path.join(__dirname, 'assets');

coreApp.use(require('node-compass')({
	mode: 'expanded',
	project: path.join(__dirname, 'assets')
}));

coreApp.use(express.static(adminAssetPath));

//load controllers
require('./controllers/admin-controller');
