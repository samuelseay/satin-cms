//TODO set up 404 routing
var _ = require('underscore'),
	express = require('express'),
	debug = require('debug')('satin'),
	path = require('path');

module.exports = function(config) {
	var self = this;

	this.name = config.name || 'base';
	this.app = config.app;
	this.router = express.Router();

	//initialize routes
	if ( config.routes ) {
		//TODO wrap route.action in a function so we can perform additional tasks before action
		//TODO one of the tasks to perform would be to turn req.params.<whatever> into top level variables and pass them to the page?
		config.routes.forEach(function(route) {
			self.router.route(route.url)[route.requestType.toLowerCase()](route.action);
			debug('route registered: ' + route.url);
		});

		self.app.use('/', self.router);
	}
};