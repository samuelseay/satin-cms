var yaml = require('yamljs'),
	file = require('./file'),
	Q = require('q');

var YAML = exports = module.exports = {};

var FRONTMATTER_REGEX = /[\n]*[-]{3}[\n]/;

YAML.parseYAMLString = function(YAMLString) {
	var	yamlObj,
		deferred = Q.defer();

	if (this.containsTabs(YAMLString)) {
		YAMLString = this.tabsToSpaces(YAMLString);
	}

	try {
		yamlObj = yaml.parse(YAMLString);
	}
	catch(e) {
		deferred.reject(new Error(e));
	}

	deferred.resolve(yamlObj);

	return deferred.promise;
}

//specifically to get Frontmatter from mixed frontmatter + markdown files.
YAML.getFrontmatter = function(filePath) {
	var deferred = Q.defer(),
		fileContent,
		fmString,
		self = this;

	file.getFileContents(filePath).then(function(contents){
		fileContent = contents;
	}).then(function() {
		fmString = self.extractFrontmatter(fileContent);
		return self.parseYAMLString(fmString);
	}).then(function(yaml) {
		deferred.resolve(yaml);
	}).fail(function(error){
		deferred.reject(new Error(error));
	});

	return deferred.promise;
}

YAML.extractFrontmatter = function(fileContents) {
	return fileContents.split(FRONTMATTER_REGEX)[1];
}

//when you need just the content and not the YAML
YAML.stripYAML = function(fileContents) {
	return fileContents.split(FRONTMATTER_REGEX)[2];
}

//if your file contains tabs I won't punish you, but I can't have both
YAML.tabsToSpaces = function(fileContents) {
	return fileContents.replace(/\t/g, '  ');
}

YAML.containsTabs = function(fileContents) {
	return (fileContents.indexOf('\t') > -1);
}
