var fs = require('fs'),
	Q = require('q');

var File = exports = module.exports = {};

File.getFileContents = function(path) {
	var deferred = Q.defer();

	this.checkFileExists(path).then(function(exists) {
		if (exists) {
			fs.readFile(path,'utf8', function(err, data) {
				if ( err ) {
					deferred.reject(err);
				}
				else {
					deferred.resolve(data);
				}
			});
		}
		else {
			deferred.reject(new Error('file does not exist'));
		}
	})


	return deferred.promise;
}

File.checkFileExists = function(path) {
	var deferred = Q.defer();

	fs.exists(path, function(exists) {
	    deferred.resolve(exists);
	});

	return deferred.promise;
}