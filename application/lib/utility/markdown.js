var fs = require('fs'),
	file = require('./file'),
	marked = require('marked'),
	yaml = require('./yaml'),
	Q = require('q');

var Markdown = exports = module.exports = {};

Markdown.parse = function(path) {
	var deferred = Q.defer();

	file.getFileContents(path).then(function(content) {
		//first strip frontmatter
		content = yaml.stripYAML(content);

		marked(content, function(err, html){
			if (err) {
				deferred.reject(err);
			}

			deferred.resolve(html);
		});
	});

	return deferred.promise;
}