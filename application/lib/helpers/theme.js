var app = require('../../satin-app'),
	path = require('path');

var ThemeHelper = exports = module.exports = {};

ThemeHelper.getTemplatePath = function(templateName) {
	return 'templates/' + templateName + '.jade';
}

ThemeHelper.getLayoutPath = function(layoutName) {
	var layoutDir = app.get('layout_dir');

	return 'templates/layouts/' + layoutName;
}