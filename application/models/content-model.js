var Q = require('q'),
	_ = require('underscore');

var contentService = require('../data/content-data-service'),
	themeHelper = require('../lib/helpers/theme');

//by design all top level pages must be named index.md
//could be configurable perhaps?
var TOP_LEVEL_PAGE_NAME = 'index.md';

var ContentModel = exports = module.exports = {};


//this model probably belongs in a separate module
function PageContent(fm, content, config) {
	this.frontmatter = fm;
	this.page_content = content;

	//might be nice to take all variables in frontmatter and put them onto a "global" object for the view?
	//we should definitely expose variables in a nice way for views to consume

	this.config = config;

	this.getTemplatePath = function() {
		var template = '';

		if ( config.template ) {
			template = config.template;
		}
		else if ( this.frontmatter.template ) {
			template = this.frontmatter.template;
		}

		return themeHelper.getTemplatePath(template);
	}

	this.getLayoutPath = function() {
		var layout = '';

		if ( config.layout ) {
			layout = config.layout;
		}
		else if ( this.frontmatter.layout ) {
			layout = this.frontmatter.layout;
		}

		return themeHelper.getLayoutPath(layout);
	}
}

//this just returns a listing currently.
ContentModel.getPages = function() {
	return contentService.getPages();
}

//main entry point to get a page along with content and frontmatter
ContentModel.getPage = function(folder, pageName) {
	var page = pageName ? pageName + '.md' : TOP_LEVEL_PAGE_NAME,
		content,
		frontmatter,
		pageContent,
		deferred = Q.defer(),
		self = this;

	//TODO implement cache of content.
	this.getPageContent(page, folder).then(function(pageContent) {
		content = pageContent;
	},function(error){
		//404?
		deferred.reject(new Error(error));
	}).then(function() {
		return self.getPageFrontmatter(page,folder);
	}).then(function(pageFrontmatter) {
		var config;

		frontmatter = pageFrontmatter;
		config = frontmatter.config ? frontmatter.config : {};

		deferred.resolve(new PageContent(frontmatter, content, config));
	});

	return deferred.promise;
}

ContentModel.getPageContent = function(pageName, folder) {
	var qualifiedPageName = folder + '/' + pageName;
	return contentService.getPageContent(qualifiedPageName);
}

ContentModel.getPageFrontmatter = function(pageName, folder) {
	var qualifiedPageName = folder + '/' + pageName;
	return contentService.getPageFrontmatter(qualifiedPageName);
}

ContentModel.getFullPathtoPage = function(pageName, folder) {
	var qualifiedPageName = folder + '/' + pageName;
	return contentService.getPagePath(pageName);
}

//the actual model
//TODO feed all returned data from methods into this.
// function Content(frontmatter, content, config) {
// 	this.frontmatter = frontmatter;
// 	this.content = content;
	//perhaps want to derive default template from file name/location etc
// 	this.template = config.template ? config.template : '';
// }
