var express = require('express'),
    path = require('path'),
    app = express(),
    favicon = require('static-favicon');

module.exports = app;

// view engine setup
app.set('theme_dir', path.join(__dirname,'../', 'themes/default'));


//TODO separate out into a configurator with YAML
app.set('view engine', 'jade');

// app.set('view options', { layout: 'layouts/default'});
app.set('views', app.get('theme_dir'));
app.set('content_dir', path.join(__dirname,'../', 'content'));
app.set('theme','default');

app.locals.basedir = path.join(__dirname,'../', 'themes/default/layouts');

app.use(require('node-compass')({
	mode: 'expanded',
	project: path.join(__dirname, 'assets')
}));

app.use(favicon());

var appAssetPath = path.join(__dirname, '../', 'themes', app.get('theme'));
app.use(express.static(appAssetPath));

//load controllers
require('./controllers/application-controller');
