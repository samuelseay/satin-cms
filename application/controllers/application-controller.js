var Controller = require('../lib/base-controller'),
	ThemeHelper = require('../lib/helpers/theme'),
	View = require('../lib/base-view'),
	app = require('../satin-app'),
	Q = require('q');

var	ApplicationController;

ApplicationController = new Controller({
	app: app,
	name: '',
	routes: [
		{
			url: '/',
			requestType: 'GET',
			action: function(req, res) {
				//load default page/layout from theme
				res.end();
			}
		},
		{
			url: '/:content/*',
			requestType: 'GET',
			action: function(req, res) {

				var ContentModel = require('../models/content-model');

				ContentModel.getPage(req.params.content, req.params['0']).then(function(page){
					var template = page.getTemplatePath(),
						layout = page.getLayoutPath();

					//also need to munge in global vars user might want to mix in
					var pageContentView = new View(res, template);

					pageContentView.render(page);
					res.end();
				}).fail(function(error) {
					//for the meantime we can just render a 404 here, but in reality we need error handling to
					//happen lower down or we need to have error types and handle based on type. e.g. 500, 404 etc
					//also need to do some logging of attempted content retrievals and fails.
					new View(res, 'templates/404').render();
					res.end();
				});
			}
		}
	]
});

