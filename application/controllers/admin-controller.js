var Controller = require('../lib/base-controller'),
	View = require('../lib/base-view'),
	contentModel = require('../models/content-model'),
	app = require('../core-app'),
	Q = require('q'),
	AdminController;

AdminController = new Controller({
	app: app,
	name: 'admin',
	routes: [
		{
			url: '/',
			requestType: 'GET',
			action: function(req, res) {
				var self = this;

				contentModel.getPages().then(function(pages){
					var indexView = new View(res,'admin/index');
					indexView.render({
						title: 'Hello Admin World',
						pages: pages
					});
				});
			}
		},
		{
			url: '/page/:page',
			requestType: 'GET',
			action: function(req, res) {
				contentModel.getPageContent(req.params.page).then(function(content) {
					var pageView = new View(res, 'admin/page');

					pageView.render({
						title: 'Page Content',
						content: content
					});
				});
			}
		}
	]
});

