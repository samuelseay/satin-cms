//TODO add grunt todo :)
var fs = require('fs'),
	path = require('path'),
	Q = require('q'),
	markdown = require('../lib/utility/markdown'),
	yaml = require('../lib/utility/yaml'),
	file = require('../lib/utility/file'),
	//TODO turn everything into modules to avoid large relative requires
	app = require('../satin-app'),
	contentDataService = exports = module.exports = {};

//TODO change naming to getPagesListing or similar
contentDataService.getPages = function() {
	var deferred = Q.defer(),
		pageList = [];

	//TODO split this out into file utility
	fs.readdir(app.get('content_dir'), function(err, files) {

		files.forEach(function(file, index, files){
			var fullPath = path.resolve(app.get('content_dir'), file);

			fs.stat(fullPath,function(err, stat) {
				if ( stat.isDirectory() ) {
					pageList.push(file);
				}

				if ( index === files.length - 1 ) {
					deferred.resolve(pageList);
				}
			});
		});

	});

	return deferred.promise;
}

contentDataService.getPageContent = function(pageName) {
	var deferred = Q.defer(),
		fullPath = this.getPagePath(pageName);

	file.checkFileExists(fullPath).then(function(exists){
		if (exists) {
			markdown.parse(fullPath).then(function(content) {
				deferred.resolve(content);
			});
		}
		else {
			//this will need to be handled as a 404
			//maybe custom errors that can be handled by express would be useful?
			deferred.reject(new Error('could not find content'));
		}
	});

	return deferred.promise;
}

contentDataService.getPageFrontmatter = function(pageName) {
	var deferred = Q.defer(),
		fullPath = this.getPagePath(pageName);

	yaml.getFrontmatter(fullPath).then(function(yamlObj) {
		deferred.resolve(yamlObj);
	});

	return deferred.promise;
}

contentDataService.getPagePath = function( pageName ) {
	return path.resolve(app.get('content_dir'), pageName);
}