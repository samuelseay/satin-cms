var	chai = require("chai"),
	assert = chai.assert,
  chaiAsPromised = require("chai-as-promised"),
	yaml = require("../../application/lib/utility/yaml"),
	path = require("path"),
	Q = require("q");

chai.use(chaiAsPromised);
chai.should();

describe('YAML Utility', function(){
	//todo, for promise returning tests should start with beforeEach to do the action then have
	//multiple it's to assert various things rather than asserting one thing per call.
	//also todo: jscoverage https://github.com/visionmedia/node-jscoverage
	describe('#getFrontmatter', function() {
		it('should extract YAML frontmatter into a YAML object from an md file with tabs', function() {
			var filePath = path.resolve('test/fixtures/content/tabs.md'),
			frontmatter;

			return yaml.getFrontmatter(filePath).should.eventually
						.have.property('title').and.equal('My Webpage');
		})

		it('should reject a YAML file with incorrect indentation', function() {
    		var filePath = path.resolve('test/fixtures/content/spaces-and-tabs.md');

    		return assert.isRejected(yaml.getFrontmatter(filePath), "Should reject invalid YAML");
		})

		it('should reject a non-existent file path', function() {
			var filePath = path.resolve('theres/no/way/this/path/could/exist/right.md');

			assert.isRejected(yaml.getFrontmatter(filePath), "Should reject invalid file path");
		})
	})

	describe('#extractFrontmatter', function() {
		it('should properly extract frontmatter from an md file', function() {
			var filePath = path.resolve('test/fixtures/content/tabs.md'),
				frontmatter;

			frontmatter = yaml.extractFrontmatter(
				'---\n' +
				'test: value\n' +
				'---\n' +
				'This is some markdown content, not to be confused with YAML'
			);

			assert.equal('test: value', frontmatter);
		})
	})

	describe('#stripYAML', function() {
		it('should strip YAML and only return the content', function() {
			var filePath = path.resolve('test/fixtures/content/tabs.md'),
				content;

			content = yaml.stripYAML(
				'---\n' +
				'test: value\n' +
				'---\n' +
				'This is some markdown content, not to be confused with YAML'
			);

			assert.equal('This is some markdown content, not to be confused with YAML', content);
		})
	})

	describe('#tabsToSpaces', function() {
		it('should replace all the tabs in a string with 2 spaces', function() {
			var tabsSpaces = '\t\t\thello\t\t\t';

			assert.equal(yaml.tabsToSpaces(tabsSpaces), '      hello      ');
		});
	})

	describe('#containsTabs', function() {
		it('should correctly detect tabs in strings', function() {
		var hasTabs = '\tThere is a tab in here',
			doesNotHaveTabs = 'I can\'t see a single tab!';

			assert.isTrue(yaml.containsTabs(hasTabs));
			assert.isFalse(yaml.containsTabs(doesNotHaveTabs));
		})
	})
})